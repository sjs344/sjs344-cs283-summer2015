#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>

volatile int worker = 0;
pthread_mutex_t    mutex = PTHREAD_MUTEX_INITIALIZER;

unsigned GetTickCount()
{
        struct timeval tv;
        if(gettimeofday(&tv, NULL) != 0)
                return 0;

        return (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
}


void * worker_thread(void * params)
{
    for (int i = 0; i < 1000; i ++)
    {
        pthread_mutex_lock(&mutex);
        worker+= 1;
        pthread_mutex_unlock(&mutex);
    }
}

int non_mutex()
{
    pthread_t thread[100];
    for (int i = 0; i < 100; i++)
    {
        if( pthread_create( &thread[i] , NULL ,  worker_thread , NULL) < 0)
        {
            perror("could not create thread");
            return 1;
        }
    }
//    for (int i = 0; i < 100; i++)
//    {
//        pthread_join(thread[i], NULL);
//    }
    printf("Finished\n");
    return 0;
}

int correct_version()
{
    pthread_t thread[100];
    for (int i = 0; i < 100; i++)
    {
        if( pthread_create( &thread[i] , NULL ,  worker_thread , NULL) < 0)
        {
            perror("could not create thread");
            return 1;
        }
    }
    for (int i = 0; i < 100; i++)
    {
        pthread_join(thread[i], NULL);
    }
    printf("Finished\n");
    return 0;
}
int mutex_inside()
{
    pthread_t thread[100];
    for (int i = 0; i < 100; i++)
    {
        if( pthread_create( &thread[i] , NULL ,  worker_thread , NULL) < 0)
        {
            perror("could not create thread");
            return 1;
        }
    }
//    for (int i = 0; i < 100; i++)
//    {
//        pthread_join(thread[i], NULL);
//    }
    printf("Finished\n");
    return 0;
}

int main()
{
    int startTime = GetTickCount();
    correct_version();
    int endTime = GetTickCount() - startTime;
    printf("Final execution time (ms) : %i , counter result %i \n", endTime, worker);
    return 0;
}
