/*
 * hw3.c
 *
 *  Created on: Mar 10, 2013
 * 	Modified on 7/22/2015
 *      Author: lou
 * 		Modified: Sam SHannon
 */

#define VIS_SIZE 10
#include "hw3.h"

/**
 * Loads an image (stored as an IplImage struct) for each
 * filename provided.
 * @param numImages		The number of images to load
 * @param filenames		A list of strings of the filenames to load
 */
IplImage** loadImages(int numImages, char ** fileNames) {
	IplImage** rv; // the return result
	int i; // used for looping

	//allocate an array of numImages IplImage* s
	rv = (IplImage**)malloc(numImages * sizeof(IplImage*));
	//iterate over each filename
	for (i = 0;i < numImages; i++)
	{
		// TODO: load the image via the cvLoadImage function
		rv[i] = cvLoadImage(fileNames[i], CV_LOAD_IMAGE_UNCHANGED);
		if (rv[i] == NULL)
			return NULL;
		//check if the file could not be loaded.
		// if it could not, print an error to stderr and return NULL
	}
	return rv;
	//return results
}

/**
 * Computes the distance between two colors (stored as CvScalars).
 *
 * Given a CvScalar c, you can access the blue, green, and red (BGR) elements
 * via c.val[0], c.val[1], and c.val[2], respectively.
 *
 * This function computes the distance between two colors as the euclidean
 * distance between the two BGR vectors.
 *
 * @see http://en.wikipedia.org/wiki/Euclidean_distance
 *
 * @param c1	The first color
 * @param c2	The second color
 * @returns		The euclidean distance between the two 3-d vectors
 */
double colorDistance(CvScalar c1, CvScalar c2) {
	double d = 0; // the result
	int i; // an iterator

	// iterate over the dimensions and compute the sum
	for (i = 0; i <  3; i++)
	{
		d += pow(c2.val[i] - c1.val[i], 2.0);	
	}
	// return the square root of the result.
	// If d is zero, just return 0.
	if (d == 0)
		return 0;
	d = sqrt(d);
	return d;
}	

/**
 * Splits up an image into numColumns by numRows sub-images and returns the results.
 *
 * @param src	The source image to split
 * @param numColumns	The number of columns to split into
 * @param numRows 		The number of rows to split into
 * @returns				A numColumns x numRows array of IplImages
 */
IplImage ** getSubImages(IplImage* src, int numColumns, int numRows) {
	int cellWidth, cellHeight, y, x, i;
	IplImage ** rv;
	CvSize s = cvGetSize(src);

	// Compute the cell width and the cell height
	cellWidth = s.width / numColumns;
	cellHeight = s.height / numRows;

	// Allocate an array of IplImage* s
	rv = malloc(sizeof(IplImage*) * numColumns * numRows);
	if (rv == NULL) {
		fprintf(stderr, "Could not allocate rv array\n");
	}

	// Iterate over the cells
	i = 0;
	for (y = 0; y < s.height; y += cellHeight)
		for (x = 0; x < s.width; x += cellWidth) {
			// Create a new image of size cellWidth x cellHeight and
			// store it in rv[i]. The depth and number of channels should
			// be the same as src.
			rv[i] = cvCreateImage(cvSize(cellWidth, cellHeight), src->depth,
					src->nChannels);
			if (rv[i] == NULL) {
				fprintf(stderr, "Could not allocate image %d\n", i);
			}

			// set the ROI of the src image
			cvSetImageROI(src, cvRect(x, y, cellWidth, cellHeight));

			// copy src to rv[i] using cvCopy, which obeys ROI
			cvCopy(src, rv[i], NULL);

			// reset the src image roi
			cvResetImageROI(src);

			// increment i
			i++;
		}

	// return the result
	return rv;
}

/**
 * Finds the CvScalar in colors closest to t using the colorDistance function.
 * @param t		 	The color to look for
 * @param scolors	The colors to look through
 * @param numColors	The length of scolors
 * @returns			The index of scolors that t is closest to
 * 					(i.e., colorDistance( t, scolors[result]) <=
 * 					colorDistance( t, scolors[i]) for all i != result)
 */
int findClosest(CvScalar t, CvScalar* scolors, int numColors) {
	int rv = 0, // return value
		 i; // used to iterate
	double d, // stores the result of distance
	       m = colorDistance(t, scolors[0]); // the current minimum distance
	// iterate over scolors
	for (i = 0; i < numColors; i++)
	{
		d = colorDistance(t, scolors[i]);
		//printf("Color distance %d -- %d -- %d \n", d, t->val[0]);
		// compute the distance between t and scolors[i]
		if (d < m)
		{
			rv = i;
			m = d;
		}
		//check if the distance is less then current minimum
		// if it is, store i as the current result and cache the minimum distance
	}
//	printf("Color distance found %d\n", m);
	return rv;
}

/**
 * For each image provided, computes the average color vector
 * (represented as a CvScalar object).
 *
 * @param images	The images
 * @param numImages	The length of images
 * @returns 		An numImages length array of CvScalars where rv[i] is the average color in images[i]
 */
CvScalar* getAvgColors(IplImage** images, int numImages) {
	CvScalar* rv;
	int i;
	//create return vector
	rv = malloc(sizeof(CvScalar) * numImages);
	//iterate over images and compute average color
	for (i=0;i<numImages;i++)
	{
		rv[i] = cvAvg(images[i], NULL);
	}
		//for each image, compute the average color (hint: use cvAvg)

	// return result
	return rv;
}

/**
 * Given an ordered list of images (iclosest), creates a
 * numColumns x numRows grid in a new image, copies each image in, and returns the result.
 *
 * Thus, if numColumns is 10, numRows is 5, and each iclosest image is 64x64, the resulting image
 * would be 640x320 pixels.
 *
 * @param iclostest		A numColumns x numRows list of images in row-major order to be put into the resulting image.
 * @param numColumns  	Number of horizontal cells
 * @param numRows		Number of vertical cells
 */
IplImage* stitchImages(IplImage** iclosest, int numColumns, int numRows) {
	int j, // for iterating over the rows
	    i, // for iterating over the columns
		cell; //cell number
	cell = 0;
	CvSize size; //for storing size of the first image 	
	//  using cvGetSize, get the size of the first image in iclosest.
	// remember all of the images should be the same size
	size = cvGetSize(iclosest[0]);
	// Compute the size of the final destination image.
	int h = size.width * numColumns;
	int w = size.height * numRows;

	// allocate the return image. This can be potentially large, so
	// you should make sure the result is not null
	IplImage* rv = cvCreateImage(cvSize(h, w), iclosest[0]->depth, iclosest[0]->nChannels);
	if (rv == NULL)
		return NULL;	
	// TODO: iterate over each cell and copy the closest image into it
	for (i = 0; i < numRows; i++)
	{
		for (j =0; j < numColumns; j++)
		{
		//	printf("Testing %i\n", iclosest[cell]->ID);
			// set the ROI of the result
			cvSetImageROI(rv, cvRect(j * size.width, i * size.height, size.width, size.height));
			// copy the proper image into the result
			cvCopy(iclosest[cell], rv, NULL);
			// reset the ROI of the result
			cvResetImageROI(rv);
			cell++;
		}
	}
	// TODO: return the result
	return rv;
}

int main(int argc, char ** argv) {
	int numberOfImages, // the number of images loaded to make the mosaic
			numColumns, // the number of horizontal images in the result
			numRows, // the number of vertical images in the result
			i; // just an iterator
	IplImage *src, // the source image
			*res, // the resulting mosaic image
			*smallRes; // the resulting mosaic image that is only 2x the size of the original
	IplImage **timages, // the small images loaded from the disk
			**subImages, // small images created from the source image
			**closest; // the pictures in timates closest to the subImages
	CvScalar *tcolors, // the average color of each thumbnail image
			*scolors; // the average color of the sub images of the source image
	// Load the source image
	src = cvLoadImage(argv[1], CV_LOAD_IMAGE_COLOR);
	// get the number of rows and columns
	numColumns = atoi(argv[2]);
	numRows = atoi(argv[3]);
	// make sure we have an even separation
	assert(src->width%numColumns==0);
	assert(src->height%numRows==0);
	// The number of images passed in
	numberOfImages = argc - 4;
	// Load the thumbnail images
	printf("Loading images\n");
	if ((timages = loadImages(numberOfImages, argv + 4)) == NULL ) {
		fprintf(stderr, "Could not load images!");
		return EXIT_FAILURE;
	}
	printf("Images loaded\n");
	// compute the average colors of the loaded thumbnails
	tcolors = getAvgColors(timages, numberOfImages);
	printf("Computing sub images - average colors\n");
	// create the sub images
	subImages = getSubImages(src, numColumns, numRows);
	// compute the average colors of the sub images
	printf("Computing sub image colors\n");
	scolors = getAvgColors(subImages, numColumns * numRows);
	printf("Finding closest images\n");
	// get the closest image to each subimage
	closest = malloc(sizeof(IplImage*) * numColumns * numRows);
#pragma omp parallel for
	for (i = 0; i < numColumns * numRows; i++) {
		int ci = findClosest(scolors[i], tcolors, numberOfImages);
		closest[i] = timages[ci];
	}
	printf("Closest Images found\n");
	// stitch the result
	res = stitchImages(closest, numColumns, numRows);
	printf( "Images stitched\n");
	// create a smaller version of the result for visualization
	smallRes = cvCreateImage(cvSize((src->width)*VIS_SIZE, (src->height)*VIS_SIZE), src->depth, src->nChannels);
	cvResize(res, smallRes, CV_INTER_CUBIC);
	printf("saving scaled image to out.jpg\n");
	cvSaveImage("out.jpg", smallRes, NULL ); 
	return EXIT_SUCCESS;
}
