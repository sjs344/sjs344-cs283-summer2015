/*
    Sam Shannon
    Assignment 3
    8/1
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

typedef int bool;
#define true 1
#define false 0

struct fileNode {
    char fullPath[255];
    char shortPath[255];
    struct stat stat;
    struct fileNode *next;
};

void getDirList(char * dir_name, struct fileNode ** fNode)
{
    DIR *directory;
    struct dirent *de;
    struct fileNode * headNode = *fNode;
    struct fileNode * lastNode = NULL;

    headNode->next = NULL;

    if (!(directory = opendir(dir_name)))
        perror("Failed to open directory");

    while (0 != (de = readdir(directory))) {
        char tmpPath[255];
        struct stat tmpStat;
        sprintf(tmpPath, "%s/%s", dir_name, de->d_name);
        stat(tmpPath, &tmpStat);

        if (S_ISDIR(tmpStat.st_mode))
            continue;

        if (lastNode == NULL)
        {
            strcpy(headNode->fullPath, tmpPath);
            strcpy(headNode->shortPath, de->d_name);
            stat(headNode->fullPath, &(headNode->stat));
            lastNode = headNode;
        }
        else
        {

            lastNode->next = malloc(sizeof(struct fileNode));
            strcpy(lastNode->next->fullPath, tmpPath);
            strcpy(lastNode->next->shortPath, de->d_name);
            stat(lastNode->next->fullPath, &(lastNode->next->stat));
            lastNode = lastNode->next;
            lastNode->next = NULL;
        }
    }
    return NULL;
}

int findPos(char * str, char search)
{
    for (int i=0; i < strlen(str); i++)
    {
        if (str[i] == search)
            return i;
    }
    return -1;
}

int findPosStr(char * str, char * search)
{
    int pos = -1;

    for (int i=0; i <= strlen(str) - strlen(search); i++)
    {
        int j;
        bool found = false;
        for (j = 0; j < strlen(search); j++)
        {
            //printf("Checking if %i matches %i\n", str[i + j], search[j]);
            if (str[i + j] != search[j])
            {
                found = false;
                break;
            }
            else
                found = true;
        }
        if (found == true)
            pos = i;
    }
    return pos;
}

char * replaceWithPattern(char * replace, char * pattern, char * str, int pos)
{
    char * newStr = malloc(strlen(str));
    strcpy(newStr, str);
    memcpy(newStr + pos, replace, strlen(replace));

    return newStr;
}

bool readReplaceBefore(char * fileName, char * before, char * pattern)
{
    FILE* fhA, * fhB;
    fhA = fopen(fileName, "r");
	bool found = false;
    char * newFileName = malloc(strlen(fileName) + 5);
    sprintf(newFileName, "%s.new", fileName);

    fhB = fopen(newFileName, "ab+");

    const size_t line_size = 300;
    char* line = malloc(line_size);
    while (fgets(line, line_size, fhA) != NULL){
        printf(line);
        printf("Finding pattern %s \n", pattern);
        int patternPos = findPosStr(line, pattern);
        if (patternPos == -1) //Only continue if pattern is not found
        {
            printf("Pattern Not Found \n");
            int beforePos = findPosStr(line, before);
            if (beforePos != -1) //Only continue if string that u want new string to put before is found
            {
                printf("Modifying and inserting line \n");
                char * finalBuffer = malloc(strlen(line) + strlen(before));
                sprintf(finalBuffer, "%s%s", pattern, line);
                fputs(finalBuffer, fhB);
				found = true;
				free(finalBuffer);
            }
        }
		fputs(line, fhB);
    }
    fclose(fhA);
    fclose(fhB);
	if (found == false)
		remove(newFileName);
	else
	{
    	remove(fileName); //Delete old
    	rename(newFileName, fileName); //Rename new to old
	}
}

bool matchContents(char * pattern, char * before, char * fileNames, struct fileNode ** fNode)
{
    char * newFileNames;


    if (fileNames != NULL)
    {
        newFileNames = malloc(strlen(fileNames) + 1 * sizeof(char));
        newFileNames[0] = '.';
        strcpy(newFileNames + 1, fileNames);
        printf("New file names %s\n", newFileNames);
    }
    struct fileNode * a = *fNode;
    while (a != NULL) //File Loop
    {
        printf("Processing file %s \n", a->shortPath);

        if (fileNames != NULL) //Check if file names match
        {
            int fNamePos = findPosStr(a->shortPath, newFileNames);
            if (fNamePos == -1)
            {
                printf("File name mismatch \n");
                a = a->next;
                continue;
            }
            readReplaceBefore(a->shortPath, before, pattern);
        }
        a = a->next;
    }
    return false;
}

int main(int argc, char **argv)
{
    struct fileNode * newNode = malloc(sizeof(struct fileNode));
    char * inputBefore = "bar";
    char * inputPattern = "foo";
    char * fNames = "txt";

    getDirList(".", &newNode);
    struct fileNode * a = newNode;

	printf("number of args %i\n", argc);	
    if (argc == 4)
		matchContents(argv[1], argv[2], argv[3], &newNode);
	else if (argc == 3)
		matchContents(argv[1], argv[2], NULL, &newNode);
	else
		printf("incorrect args\n");
    return 0;
}
