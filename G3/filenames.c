/*
    Sam Shannon
    Assignment 3
    8/1
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

typedef int bool;
#define true 1
#define false 0

struct fileNode {
    char fullPath[255];
    char shortPath[255];
    struct stat stat;
    struct fileNode *next;
};

void getDirList(char * dir_name, struct fileNode ** fNode)
{
    DIR *directory;
    struct dirent *de;
    struct fileNode * headNode = *fNode;
    struct fileNode * lastNode = NULL;

    headNode->next = NULL;

    if (!(directory = opendir(dir_name)))
        perror("Failed to open directory");

    while (0 != (de = readdir(directory))) {
        char tmpPath[255];
        struct stat tmpStat;
        sprintf(tmpPath, "%s/%s", dir_name, de->d_name);
        stat(tmpPath, &tmpStat);

        if (S_ISDIR(tmpStat.st_mode))
            continue;

        if (lastNode == NULL)
        {
            strcpy(headNode->fullPath, tmpPath);
            strcpy(headNode->shortPath, de->d_name);
            stat(headNode->fullPath, &(headNode->stat));
            lastNode = headNode;
        }
        else
        {

            lastNode->next = malloc(sizeof(struct fileNode));
            strcpy(lastNode->next->fullPath, tmpPath);
            strcpy(lastNode->next->shortPath, de->d_name);
            stat(lastNode->next->fullPath, &(lastNode->next->stat));
            lastNode = lastNode->next;
            lastNode->next = NULL;
        }
    }
    return NULL;
}

int findPos(char * str, char search)
{
    for (int i=0; i < strlen(str); i++)
    {
        if (str[i] == search)
            return i;
    }
    return -1;
}

int findPosStr(char * str, char * search)
{
    int pos = -1;

    for (int i=0; i <= strlen(str) - strlen(search); i++)
    {
        int j;
        bool found = false;
        for (j = 0; j < strlen(search); j++)
        {
            //printf("Checking if %i matches %i\n", str[i + j], search[j]);
            if (str[i + j] != search[j])
            {
                found = false;
                break;
            }
            else
                found = true;
        }
        if (found == true)
            pos = i;
    }
    return pos;
}

char * replaceWithPattern(char * replace, char * pattern, char * str, int pos)
{
    char * newStr = malloc(strlen(str));
    strcpy(newStr, str);
    memcpy(newStr + pos, replace, strlen(replace));

    return newStr;
}
bool matchReplace(char * pattern, char * replace, char * fileNames, struct fileNode ** fNode)
{
    int pos = findPos(pattern, '*');
    char * testStr = "111cs283.txt";
    char * newFileNames;

    printf("Found pos of * %i\n", pos);

    if (fileNames != NULL)
    {
        newFileNames = malloc(strlen(fileNames) + 1 * sizeof(char));
        newFileNames[0] = '.';
        strcpy(newFileNames + 1, fileNames);
        printf("New file names %s\n", newFileNames);
    }
    struct fileNode * a = *fNode;
    while (a != NULL)
    {
        printf("Processing file %s \n", a->fullPath);

        if (fileNames != NULL)
        {
            int fNamePos = findPosStr(a->shortPath, newFileNames);
            if (fNamePos == -1)
            {
                printf("File name mismatch \n");
                a = a->next;
                continue;
            }
        }
        if (pos == 0) //Find after *
        {
            int spos = findPosStr(a->shortPath, pattern + 1);
            if (spos != -1)
            {
                printf("Found position of pattern %s in str %s - %i\n", pattern, a->shortPath,  spos);

                char * newStr = replaceWithPattern(replace, pattern + 1, a->shortPath, spos);
                printf("New filename %s\n", newStr);
                rename(a->shortPath, newStr);
            }
            else
                printf("No pattern match in %s\n", a->shortPath);
        }
        else if (pos == -1) //no *
        {

            int spos = findPosStr(a->shortPath, pattern);
            if (spos != -1)
            {
                printf("Found position of pattern %s in str %s - %i\n", pattern, a->shortPath,  spos);
                char * newStr = replaceWithPattern(replace, pattern, a->shortPath, spos);
                printf("Renamed file to %s\n", newStr); 
				rename(a->shortPath, newStr);
            }
            else
                printf("No pattern match in %s\n", a->shortPath);
        }
        else //find before
        {
            char * newPattern = malloc(sizeof(char) * strlen(pattern));
            memcpy(newPattern, pattern, strlen(pattern) - 1);
            printf("New pattern %s \n", newPattern);

            int spos = findPosStr(a->shortPath, newPattern);
            if (spos > 0)
            {
                printf("Found position of pattern %s in str %s - %i\n", pattern, a->shortPath,  spos);

                char * newStr = replaceWithPattern(replace, newPattern, a->shortPath, spos);
                rename(a->shortPath, newStr);
                printf("Renamed file to %s\n", newStr);
            }
            else
                printf("No pattern match in %s\n", a->shortPath);
        }
        a = a->next;
    }
    return false;
}

int main(int argc, char **argv)
{
    struct fileNode * newNode = malloc(sizeof(struct fileNode));
    char * inputReplace = "cs275";
    char * inputPattern = "*cs283";
    char * fNames = "txt";

    getDirList(".", &newNode);
    struct fileNode * a = newNode;
	printf("Number of args %i \n", argc);
    matchReplace(argv[1], argv[2], argv[3], &newNode);
    return 0;
}
