#include <stdio.h>
#include <stdlib.h>

//Define an int* pointer variable, and create an array of 10 integers using malloc().  Then, assign values to that array, print their values, and free() the integers.

struct ListNode {
	int data;
	struct ListNode *next;
};
//
void questionOne()
{
	int * intArrayPtr = (int*)malloc(10 * sizeof(int));
	for (int i = 0; i < 10; i++)
	{
		intArrayPtr[i] = i;
		printf("My int: %i\n", intArrayPtr[i]);
	}
	free(intArrayPtr);
}

//Using malloc, create a char** pointer that contains 10 char*'s, then in a loop, initialize each of the 10 char*'s in a loop to a char array of size 15, and initialize each to a word of your choice (don't forget the null terminator \0) -- and print them to screen.
void questionTwo()
{
	char ** strArrayPtr = (char**)malloc(10 * sizeof(char*));
	for (int i = 0; i < 10; i++)
	{
		strArrayPtr[i] = (char*)malloc(15 * sizeof(char));
		sprintf(strArrayPtr[i], "String Value %i\n", i);
		printf(strArrayPtr[i]);
		//free(strArrayPtr[i]);
	}
	free(strArrayPtr);
}

void questionThree(int ** a, int size)
{
	int * b = *a;
	for (int i = 0; i < size; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if (b[i] > b[j])
			{
				int temp = (*a)[i];
				(*a)[i] = (*a)[j];
				(*a)[j] = temp;
			}
		}
	}
	for (int i = 0; i < size; i++)
	{
		printf("%i ", (*a)[i]);
	}
	printf("\n");
}

void displayList(struct ListNode * a)
{
	for (struct ListNode * x = a; x != NULL; x = x->next)
	{
		printf("Node value: %i\n", x->data);
	}
}

//Modify this program to take in a linked list of structs that you create (with a int data element, and a struct ListNode*), and sort the linked list.
//Note that you should swap the actual nodes and not just the values within those nodes.

void questionFour(struct ListNode ** a)
{
	struct ListNode * head = *a, *prev, *temp;
	for (struct ListNode * x = head; x != NULL; x = x->next)
	{
		struct ListNode * min = x;
//		printf("Current Node %i Node->next %i \n", x->data, x->next->data);
		for (struct ListNode *y = x; y->next != NULL; y = y->next)
		{
			if (x->data > y->next->data)
			{
				prev = y;
				min = y->next;
			}
		}
//		printf("min = %i\n", min->data);
		if (min != x)
		{
			if (x == head)
			{
				temp = head->next;
				if (min->next == NULL) //last element of list
				{
					head->next = NULL;
					prev->next = head; //link to the element being swapped
				}
				else
					prev->next = head;
				min->next = temp;
				head = min;
				*a = min; //create new head leading linked list
			}
			else
			{
				temp = x;
				prev->next = temp; //swap nodes
				temp->next = min->next;
				min->next = x->next;
			}
		}
	}
	displayList(head);
}

//Finally, write a program that, using malloc and realloc, creates an array of initial size n.Write add(), remove() and get() functions for your array.
//When adding beyond the end of the array, reallocate space such that the array contains one more element.Time your program for adding 100000 elements(or more).
//Then modify the program such that it increases in size by a factor of 2 times the previous size.Time it again.What do you observe ?

void get(int **array, int index)
{
	printf("Displaying element %i - Value %i\n", index, (*array)[index]);
}

void remove(int ** array, int index, int * size)
{
	for (int i = index; i < *size - 1; i++)
	{
		*array[i] = *array[i + 1];
	}
	int * tmp = (int*)realloc(*array, (*size - 1) * sizeof(int));
	if (tmp == NULL)
		perror("Realloc error\n");
	else
		*array = tmp;
	*size = *size - 1;
}

void add(int ** array, int value, int * size)
{
	*size = *size + 1;
	//*size = *size * 2;
	printf("Realloc to size %i\n", *size);
	int * tmp = (int*)realloc(*array, *size * sizeof(int));
	if (tmp == NULL)
		perror("Realloc error\n");
	else
		*array = tmp;
	(*array)[*size] = value;
}

void questionFive(int size)
{
	int * intArray = (int*)malloc(size * sizeof(int));
	for (int i = 0; i < 100000; i++)
	{
		add(&intArray, i, &size);
	}

}

int main()
{
	printf("Question One\n");
	questionOne();
	printf("Question Two\n");
	questionTwo();

	int * intArray = (int*)malloc(5 * sizeof(int));
	intArray[0] = 5;
	intArray[1] = 2;
	intArray[2] = 3;
	intArray[3] = 4;
	intArray[4] = 6;
	printf("Question 3\n");
	questionThree(&intArray, 5);


	struct ListNode * a = (ListNode*)malloc(sizeof(ListNode));
	struct ListNode * b = (ListNode*)malloc(sizeof(ListNode));
	a->next = b;
	a->data = 3;
	struct ListNode * c = (ListNode*)malloc(sizeof(ListNode));
	b->next = c;
	b->data = 2;
	struct ListNode * d = (ListNode*)malloc(sizeof(ListNode));
	c->next = d;
	c->data = 1;
	d->next = NULL;
	d->data = 0;
	printf("Question 4\n");
	questionFour(&a);

	printf("Question 5\n");
	questionFive(5);

	return 0;
}