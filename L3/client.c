#include <stdio.h>
#include <stdlib.h>
#include<stdio.h>
#include<sys/socket.h>
#include<arpa/inet.h>	//inet_addr

int main(int argc , char *argv[])
{
    int client = socket(AF_INET , SOCK_STREAM , 0);
    struct sockaddr_in s_addr;

	printf("Connecting to %s:80\n", argv[1]);
    s_addr.sin_addr.s_addr = inet_addr(argv[1]);
    s_addr.sin_family = AF_INET;
    s_addr.sin_port = htons( 80 );

	//client connect
    if (connect(client , (struct sockaddr *)&s_addr , sizeof(s_addr)) < 0)
    {
        perror("connect error");
        return 1;
    }
	printf("connected\n");
    char * sBuffer = "GET /index.html HTTP/1.1\r\nHost: www.google.com\r\n\r\n";
    int w = write(client, sBuffer, strlen(sBuffer));
	printf("wrote %i bytes\n", w);

    char rBuffer[256];
	int r = read(client, rBuffer, sizeof(rBuffer));
	printf("%s", rBuffer);
    while (r > 0) //read and print
    {
		r = read(client, rBuffer, sizeof(rBuffer));
        printf("%s", rBuffer);
    }
	close(client);
    return 0;
}
