#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>	//inet_addr
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

int SendFile(int client, char * path)
{
    int fdA, fdB;
    int r = 0;
    char buffer[255];

    if ((fdA = open(path, O_RDONLY)) < 0) {
       perror("open");
       exit(1);
    }

    do //Read A then write client
    {
        int r = read(fdA, buffer, sizeof(buffer));
        if (r > 0)
            write(client, buffer, sizeof(buffer));
    } while (r > 0);

    close(fdA);
	return 0;
}

int main(int argc , char *argv[])
{
	//Create socket
    int server = socket(AF_INET , SOCK_STREAM , 0);
    struct sockaddr_in s_addr, c_addr;
	s_addr.sin_family = AF_INET;
	s_addr.sin_addr.s_addr = INADDR_ANY;
	s_addr.sin_port = htons(atoi(argv[1]));
	//Bind
	if (bind(server, (struct sockaddr *) &s_addr, sizeof(s_addr)) < 0)
    {
    	perror("ERROR on binding");
    	exit(1);
    }
	listen(server, 10);
	int clientlen  = sizeof(c_addr);
	int clientfd = 0;
	do { //Accept loop
		clientfd = accept(server, (struct sockaddr *)&c_addr, &clientlen);
		if (clientfd > 0) //new client
		{
			char buffer[256];
			int r = read(clientfd, buffer, sizeof(buffer));
			//strtok to split
			char * st = strtok(buffer, " ");
			char * path = strtok(NULL, " ");//find path
			SendFile(clientfd, path); //read path and send
			close(clientfd);
		}

	} while(clientfd > 0);
	return 0;
}
