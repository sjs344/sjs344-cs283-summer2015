#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

typedef int bool;
#define true 1
#define false 0

struct wordNode {
    char * data;
    struct wordNode *next;
};
struct hashTable {
    struct wordNode * wordList;
};

int hashChar(char letter)
{
    letter = tolower(letter);
    switch (letter)
    {
        case 'a':
            return 1;
        case 'b':
            return 2;
        case 'c':
            return 3;
        case 'd':
            return 4;
        case 'e':
            return 5;
        case 'f':
            return 6;
        case 'g':
            return 7;
        case 'h':
            return 8;
        case 'i':
            return 9;
        case 'j':
            return 10;
        case 'k':
            return 11;
        case 'l':
            return 12;
        case 'm':
            return 13;
        case 'n':
            return 14;
        case 'o':
            return 15;
        case 'p':
            return 16;
        case 'q':
            return 17;
        case 'r':
            return 18;
        case 's':
            return 19;
        case 't':
            return 20;
        case 'u':
            return 21;
        case 'v':
            return 22;
        case 'w':
            return 23;
        case 'x':
            return 24;
        case 'y':
            return 25;
        case 'z':
            return 26;
        default:
            return 0;
        return 0;
    }
}

int getHashValue(char * word)
{
    int hash = 0;
    for (int i=0;i < strlen(word); i++)
    {
        hash += hashChar(word[i]);
    }
    return hash;
}

void removeNonAscii(char * word)
{
    for (int i=0;i<strlen(word) + 1;i++)
    {
        if (!( (word[i] >= 'a' && word[i] <= 'z') || (word[i] >= 'A' && word[i] <= 'Z') ))
        {
            (word)[i] = '\0';
        }
    }
}
bool processNewHash(struct wordNode ** WordNode, char * word)
{
    if (*WordNode == NULL) //HEAD NODE
    {
        *WordNode = malloc(sizeof(struct wordNode));
        (*WordNode)->data = word;
        (*WordNode)->next = NULL;
    }
    else //Other nodes
    {
        struct wordNode * currentNode = *WordNode;
        while (currentNode->next != NULL) //Find last node and add to end
            currentNode = currentNode->next;

        struct wordNode * newNode = malloc(sizeof(struct wordNode));
        newNode->next = NULL;
        newNode->data = word;
        currentNode->next = newNode;
    }
    return true;
}
int newReadDictionary(char* fileName, struct hashTable ** HashTable)
{
    FILE* dict = fopen(fileName, "r"); //open the dictionary for read-only access
    if(dict == NULL) {
        printf("read error\n");
        return 0;
    }

    // Read each line of the file, and print it to screen
    char word[128];
    int largestHash = 0;
    struct wordNode * wordList;
    struct wordNode * lastNode = NULL;
    bool headNode = true;
    while(fgets(word, sizeof(word), dict) != NULL) { //get dict size (amount of words)
        int hash = getHashValue(word);
        removeNonAscii(word);
        if (hash > largestHash) //find largest hash to try to save memory
            largestHash = hash;
        if (headNode == true) //create linked list of all dictionary values so it does not need to be oppened again
        {
            wordList = malloc(sizeof(struct wordNode));
            wordList->data = malloc(sizeof(char) * 128);
            strcpy(wordList->data, word);
            lastNode = wordList;
            headNode = false;
        }
        else
        {
            lastNode->next = malloc(sizeof(struct wordNode));
            lastNode->next->data = malloc(sizeof(char) * 128);
            strcpy(lastNode->next->data, word);
            lastNode = lastNode->next;
            lastNode->next = NULL;
        }
    }

    *HashTable = malloc(sizeof(struct hashTable) * largestHash + 2); //HashTable new
     for (int i=0; i < largestHash + 1; i++)
     {
         (*HashTable)[i].wordList = NULL;
     }
     struct wordNode * currentNode = wordList;
     while (currentNode != NULL)
     {
        char * word = currentNode->data;
        int hash = getHashValue(word); //hash = value of word
        processNewHash(&(((*HashTable)[hash]).wordList), word);
        currentNode = currentNode->next;
     }


    return 0;
}

bool isAnagram(char * word1, char * word2)
{
    if (!(strlen(word1) == strlen(word2)))
        return false;

    if (strcmp(word1, word2) == 0)
        return false;

    for (int i=0;i<strlen(word1);i++)
    {
        bool letterFound = false;
        for (int j=0;j<strlen(word2);j++)
        {
            if (word1[i] == word2[j])
            {
                 letterFound = true;
                 break;
            }
        }
        if (letterFound == false)
            return false;
    }
    return true;
}
bool findAnagrams(struct hashTable * HashTable, char * word)
{
    int hash = getHashValue(word);
    struct wordNode * wordList = HashTable[hash].wordList;
    bool isFound = false;
    while (wordList->next != NULL)
    {
        char * data = wordList->data;
        if (isAnagram(data, word))
        {
            printf("Found Anagram %s\n", data);
            isFound = true;
        }
        wordList = wordList->next;
    }
    return isFound;
}

int main(int argc, char **argv)
{
	if (argc < 1)
		return 0;

    struct hashTable * HashTable;
    newReadDictionary("/usr/share/dict/words", &HashTable);
    if (findAnagrams(HashTable,argv[1]) == false)
        printf("Sorry could not find any anagrams to %s\n", argv[1]);
    return 0;
}
