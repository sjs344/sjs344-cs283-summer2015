/*
    Sam Shannon
    Lab 2
    7/27
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

typedef int bool;
#define true 1
#define false 0

struct fileNode {
    char fullPath[255];
    char shortPath[255];
    struct stat stat;
    struct fileNode *next;
};

void getDirList(char * dir_name, struct fileNode ** fNode)
{
    DIR *directory;
    struct dirent *de;
    struct fileNode * headNode = *fNode;
    struct fileNode * lastNode = NULL;

    headNode->next = NULL;

    if (!(directory = opendir(dir_name)))
        perror("Failed to open directory");

    while (0 != (de = readdir(directory))) {
        char tmpPath[255];
        struct stat tmpStat;
        sprintf(tmpPath, "%s/%s", dir_name, de->d_name);
        stat(tmpPath, &tmpStat);

        if (S_ISDIR(tmpStat.st_mode))
            continue;

        if (lastNode == NULL)
        {
            strcpy(headNode->fullPath, tmpPath);
            strcpy(headNode->shortPath, de->d_name);
            stat(headNode->fullPath, &(headNode->stat));
            lastNode = headNode;
        }
        else
        {

            lastNode->next = malloc(sizeof(struct fileNode));
            strcpy(lastNode->next->fullPath, tmpPath);
            strcpy(lastNode->next->shortPath, de->d_name);
            stat(lastNode->next->fullPath, &(lastNode->next->stat));
            lastNode = lastNode->next;
            lastNode->next = NULL;
        }
    }
    return NULL;
}

void replicate(char* pathA, char* pathB)
{
    int fdA, fdB;
    int r = 0;
    char buffer[255];

    if ((fdA = open(pathA, O_RDONLY)) < 0) {
       perror("open");
       exit(1);
    }
    if ((fdB = open(pathB, O_CREAT | O_WRONLY)) < 0) {
       perror("open");
       exit(1);
    }
    do //Read A then write in B in same buffer, saves time
    {
        int r = read(fdA, buffer, sizeof(buffer));
        if (r > 0)
            write(fdB, buffer, sizeof(buffer));
    } while (r > 0);

    close(fdA);
    close(fdB);
}

void deleteB(struct fileNode * aFileNode, struct fileNode * bFileNode)
{
    struct fileNode * a = aFileNode;
    struct fileNode * b = bFileNode;
    bool found = false;

    while (b != NULL)
    {
        found = false;
        a = aFileNode;
        while (a != NULL)
        {
            if (strcmp(a->shortPath, b->shortPath) == 0)
                found = true;
            a = a->next;
        }
        if (found == false)
        {
            int r = remove(b->fullPath);
            printf("File found in B does not exist in A, Deleting %s. Status %i", b->fullPath, r);
        }
        b = b->next;
    }
}

void replicateB(struct fileNode * aFileNode, struct fileNode * bFileNode, char * dirB)
{
    struct fileNode * a = aFileNode;
    struct fileNode * b = bFileNode;
    bool found = false;

    while (a != NULL)
    {
        found = false;
        b = bFileNode;
        while (b != NULL)
        {
            if (strcmp(a->shortPath, b->shortPath) == 0)
                found = true;
            b = b->next;
        }
        if (found == false)
        {
            char fullPathB[255];
            sprintf(fullPathB, "%s/%s", dirB, a->shortPath);
            printf("File found in A not in B, Copying %s to %s \n", a->fullPath, fullPathB);
            replicate(a->fullPath, fullPathB);
        }
        a = a->next;
    }
}

void copyRecent(struct fileNode * aFileNode, struct fileNode * bFileNode)
{
    struct fileNode * a = aFileNode;
    struct fileNode * b = bFileNode;
    bool found = false;
    int r = 0;
//    struct tm at, bt;

    while (a != NULL)
    {
        found = false;
        b = bFileNode;
        while (b != NULL)
        {
            if (strcmp(a->shortPath, b->shortPath) == 0)
            {
                found = true;
                break;
            }
            b = b->next;
        }
        if (found == true)
        {
            if (a->stat.st_mtime > b->stat.st_mtime) //modified time in a is more recent than b
            {
                r = remove(b->fullPath);
                replicate(a->fullPath, b->fullPath);
                printf("File %s has more recent modified time than %s, replaced %s. Status: %i \n", a->fullPath, b->fullPath, b->fullPath, r);

            }
            else
            {
                r = remove(a->fullPath);
                replicate(b->fullPath, a->fullPath);
                printf("File %s has more recent modified time than %s, replaced %s. Status: %i \n", b->fullPath, a->fullPath, a->fullPath, r);
            }
        }
        a = a->next;
    }
}

int main()
{
    struct fileNode * aFileNode = malloc(sizeof(struct fileNode));
    struct fileNode * bFileNode = malloc(sizeof(struct fileNode));

    bFileNode->next = NULL;
    aFileNode->next = NULL;

    getDirList("a", &aFileNode);
    getDirList("b", &bFileNode);

    struct fileNode * a = aFileNode;
    while (a != NULL)
    {
        printf("File %s last modified %i \n", a->fullPath, a->stat.st_mtime);
        a = a->next;
    }
    //Replicate files in A not found in B
    replicateB(aFileNode, bFileNode, "b");
    //Delete files in B not found in A
    deleteB(aFileNode, bFileNode);
    //Copy most recent
    copyRecent(aFileNode, bFileNode);

    return 0;
}
