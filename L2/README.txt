Sam Shannon 
CS283
L2
7/27


NOTE: Tux wouldn't let me login so I couldn't make a makefile for this or do any of the parameter stuff. My code is correct though it works on my machine using CodeBlocks and Windows 7 64 bit.

Included are my main.c and sample directories with a few files. I implemented everything asked for in L2 and it all works. I used linked lists to store information about each file.
For each directory, it searches through each linked list then finds files, compares, and does whatever is necessary for the task.

I did not find this especially difficult as I have worked with file descriptors before when coding networking stuff in c. I did not find any of the robust IO stuff needed and found my own
implemenation better than anything that I would have used on there.