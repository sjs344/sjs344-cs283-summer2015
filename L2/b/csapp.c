/* Updated 4/18/13 droh: 
 *   rio_readlineb: fixed edge case bug
 *   rio_readnb: removed redundant EINTR check
 */
/* $begin csapp.c */
#include "csapp.h"

/************************** 
 * Error-handling functions
 **************************/
/******/
/