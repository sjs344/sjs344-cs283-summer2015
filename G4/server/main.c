#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <arpa/inet.h>	//inet_addr
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <netdb.h>
#include <netinet/in.h>
#include "../minirsa.h"

typedef int bool;
#define true 1
#define false 0

int e = 0; int d = 0; int c = 0; int dc = 0;

bool SetSocketBlockingEnabled(int fd, bool blocking)
{
   if (fd < 0) return false;

#ifdef WIN32
   unsigned long mode = blocking ? 0 : 1;
   return (ioctlsocket(fd, FIONBIO, &mode) == 0) ? true : false;
#else
   int flags = fcntl(fd, F_GETFL, 0);
   if (flags < 0) return false;
   flags = blocking ? (flags&~O_NONBLOCK) : (flags|O_NONBLOCK);
   return (fcntl(fd, F_SETFL, flags) == 0) ? true : false;
#endif
}

void * connection_thread(void * param)
{
    int client = *(int*)param;
    fd_set active_fd_set;

    int r = 0; int len = 0;
    FD_ZERO(&active_fd_set);
    FD_SET(client, &active_fd_set);
    FD_SET(fileno(stdin), &active_fd_set);

    do
    {
        printf("Type Message Below: \n");
        int s = select(FD_SETSIZE, &active_fd_set, NULL, NULL, NULL);
        for (int i = 0; i < FD_SETSIZE; i++)
        {
            if (FD_ISSET(i, &active_fd_set))
            {
                if (i == client)
                {
                    int bytes_recv = 0;
                    int r = read(client, &len, sizeof(len));
                    int * encrypted_bytes = malloc(sizeof(int) * len);
                    if (r <= 0)
                        break;
                    while (bytes_recv < len)
                    {
                        r = read(client, encrypted_bytes + bytes_recv, len - bytes_recv);
                        bytes_recv += r;
                    }
                    //char * RSADecrypt(int * message, int message_len, int d, int dc);
                    char * message = RSADecrypt(encrypted_bytes, len, d, dc);
                    message[len] = '\0';
                    printf("Received Message %s \n", message);
                    printf("Say something: \n");
                }
                else if (i == fileno(stdin))
                {
                    char buffer[255];
                    int r = read(STDIN_FILENO, buffer, 254);
                    printf("Sending message %s \n, buffer");
                    int * message = RSAEncrypt(buffer, &r, e, c);
                    int w = write(client, &r, sizeof(r));
                    w = write(client, &message, r);
                }
            }
        }
        FD_SET(client, &active_fd_set);
        FD_SET(fileno(stdin), &active_fd_set);


    } while (r > 0);

    printf("Client %i disconnected \n", client);
    close(client);
}
int main(int argc , char *argv[])
{
	//Create socket

    int server = socket(AF_INET , SOCK_STREAM , 0);
    struct sockaddr_in s_addr;
    struct sockaddr_in c_addr;
    bzero(&s_addr, sizeof(s_addr));
	s_addr.sin_family = AF_INET;
	s_addr.sin_addr.s_addr = INADDR_ANY;
	s_addr.sin_port = htons(atoi(argv[1]));
	//Bind


    e = atoi(argv[2]);
    d = atoi(argv[3]);
    c = atoi(argv[4]);
    dc = atoi(argv[5]);
	if (bind(server, (struct sockaddr *) &s_addr, sizeof(s_addr)) < 0)
    {
    	perror("ERROR on binding");
    	exit(1);
    }
    printf("Binded to port %s \n", argv[1]);
	listen(server, 5);
	int clientlen  = sizeof(c_addr);
	do { //Accept loop
        printf("Waiting for clients \n");
		int clientfd = accept(server, (struct sockaddr *)&c_addr, &clientlen);
		if (clientfd > 0) //new client
		{
            printf("Accepted new client on socket %i \n", clientfd);
            pthread_t thread;
            if( pthread_create( &thread , NULL ,  connection_thread , &clientfd) < 0)
            {
                perror("could not create thread");
                return 1;
            }
		}

	} while(server > 0);
	return 0;
}
