#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <arpa/inet.h>	//inet_addr
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <netdb.h>
#include <netinet/in.h>
#include "../minirsa.h"

typedef int bool;
#define true 1
#define false 0

int e = 0; int d = 0; int c = 0; int dc = 0;

bool SetSocketBlockingEnabled(int fd, bool blocking)
{
   if (fd < 0) return false;

#ifdef WIN32
   unsigned long mode = blocking ? 0 : 1;
   return (ioctlsocket(fd, FIONBIO, &mode) == 0) ? true : false;
#else
   int flags = fcntl(fd, F_GETFL, 0);
   if (flags < 0) return false;
   flags = blocking ? (flags&~O_NONBLOCK) : (flags|O_NONBLOCK);
   return (fcntl(fd, F_SETFL, flags) == 0) ? true : false;
#endif
}

int main(int argc , char *argv[])
{

    int client = socket(AF_INET , SOCK_STREAM , 0);
    struct sockaddr_in s_addr;

	printf("Connecting to %s:%s\n", argv[1], argv[2]);
    s_addr.sin_addr.s_addr = inet_addr(argv[1]);
    s_addr.sin_family = AF_INET;
    s_addr.sin_port = htons( atoi(argv[2]) );

    e = atoi(argv[3]);
    d = atoi(argv[4]);
    c = atoi(argv[5]);
    dc = atoi(argv[6]);
	//client connect
    if (connect(client , (struct sockaddr *)&s_addr , sizeof(s_addr)) < 0)
    {
        perror("connect error");
        return 1;
    }

    fd_set active_fd_set;

    int r = 0; int len = 0;
    FD_ZERO(&active_fd_set);
    FD_SET(client, &active_fd_set);
    FD_SET(fileno(stdin), &active_fd_set);
    int s = 0;
    do
    {
        printf("Type Message Below: \n");
        s = select(FD_SETSIZE, &active_fd_set, NULL, NULL, NULL);
        for (int i = 0; i < FD_SETSIZE; i++)
        {
            if (FD_ISSET(i, &active_fd_set))
            {
                if (i == client)
                {
                    printf("Client data to process\n");
                    int bytes_recv = 0;
                    int r = read(client, &len, sizeof(len));

                    if (r <= 0)
                        s = 0;
                    int * encrypted_bytes = malloc(sizeof(int) * len);
                    while (bytes_recv < len)
                    {
                        r = read(client, encrypted_bytes + bytes_recv, len - bytes_recv);
                        bytes_recv += r;
                    }
                    //char * RSADecrypt(int * message, int message_len, int d, int dc);
                    char * message = RSADecrypt(encrypted_bytes, len, d, dc);
                    printf("Received Message %s \n", message);
                    printf("Say something: \n");
                }
                else if (i == fileno(stdin))
                {
                    printf("STDin Process\n");
                    char buffer[255];
                    int r = read(STDIN_FILENO, buffer, 255);
                    printf("Sending message %s \n, buffer");
                    int * message = RSAEncrypt(buffer, &r, e, c);
                    int w = write(client, &r, sizeof(r));
                    w = write(client, &message, r);
                }
            }
        }
        FD_SET(client, &active_fd_set);
        FD_SET(fileno(stdin), &active_fd_set);
    } while (s > 0);
	close(client);

    return 0;
}
