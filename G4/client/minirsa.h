#ifndef MINIRSA_H_INCLUDED
#define MINIRSA_H_INCLUDED

char * RSADecrypt(int * message, int message_len, int d, int dc);
int * RSAEncrypt(char * message, int * message_len, int e, int c);
#endif // MINRSA_H_INCLUDED
