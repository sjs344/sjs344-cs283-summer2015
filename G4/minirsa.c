#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define INT_MAX 32767
#include "minirsa.h"

int GCD(int a, int b)
{
    printf("Finding GCD between %i , %i \n", a, b);
    while( 1 )
    {
        a = a % b;
        if( a == 0 )
            return b;
        b = b % a;
        if( b == 0 )
            return a;
    }
}

int coprime(int x, int y)
{
    int a = y * 2;
    while (1)
    {
        if (GCD(a, x) == 1)
            if (GCD(a, y) == 1)
                return a;
        a++;
    }
    return 0;
}

int powerMod(int x, int p, int N)
{
    int A = 1;
    int m = p;
    int t = x;

    while (m > 0)
    {
        int k = floor(m/2);
        int r = m - 2*k;
        if (r == 1)
            A = (A*t) % N;
        t = (t * t) % N;
        m = k;
    }
    return A;
}
int endecrypt(int msg_or_cipher, int key, int c)
{
    int result = powerMod(msg_or_cipher, key, c);
    printf("result = %i\n", result);
    return result;
}

int mod_inverse(int base, int m)
{
    base %= m;
    for(int x = 1; x < m; x++) {
        if((base*x) % m == 1) return x;
    }
}

int check_prime(int check)
{
    int x = 0;
    for (x = 2; x <= check - 1; x++)
    {
        if (check % x == 0)
            return 0;
    }
    if (x == check)
        return 1;
}

int findPrimeTo(int lastPrime)
{
    int x = 0;
    for (x = lastPrime; x > 2; x--)
    {
        printf("checking... %i\n", x);
        if (check_prime(x) == 1)
        {
            printf("x is prime %i \n", x);
            return x;
        }
    }
    return 0;
}
void findPrimes(int * x, int * y)
{
    time_t t;
    srand((unsigned) time(&t));
    *x = findPrimeTo(*x);
    *y = findPrimeTo(*y);
}

void get_factors(int * factor1, int * factor2, int num_to_factor)
{
    int i = 1;
    int n = num_to_factor;
    int x = 1;
    while (num_to_factor > 1 && ++i <= sqrt(n))
    {
        while (num_to_factor%i == 0)
        {
            num_to_factor = num_to_factor / i;
            *factor1 += i;
            printf("searching... %i %i\n", num_to_factor, i);
        }
    }
    *factor2 = num_to_factor;
    printf("factor1 = %i factor2 = %i \n", *factor1, *factor2);
}
int candidates(int m, int * e, int * c)
{
    int r = m;
    int n = m + 1;

    while (n < INT_MAX)
    {
        n = n + r;
        if (check_prime(n) == 0)
        {
            printf("checking %i for factors \n", n);
            get_factors(e, c, n);
            return 0;
        }
    }
}

int * RSAEncrypt(char * message, int * message_len, int e, int c)
{
    *message_len = sizeof(int) * (*message_len);
    int * result = malloc(*message_len * sizeof(int));
    for (int i = 0; i < *message_len; i++)
    {
        result[i] = endecrypt(message[i], e, c);
    }
    return result;
}

char * RSADecrypt(int * message, int message_len, int d, int dc)
{
    int array_size = sizeof(char) * (message_len);
    char * result = malloc(array_size);
    for (int i = 0; i < message_len; i++)
    {
        result[i] = endecrypt(message[i], d, dc);
    }
    return result;
}

int startRSA(int prime1, int prime2)
{
    printf("StartRSA x = %i, y = %i\n", prime1, prime2);
    int c = prime1 * prime2;
    printf("c = %i \n", c);
    int m = (prime1 - 1) * (prime2 -1);
    printf("m = %i \n", m);
    int e = 0; int d = 0;
    candidates(m, &e, &d);
    int f = endecrypt(15, e, c);
    int g = endecrypt(f, d, c);
    printf("Encrypted : %i Decrypted : %i\n", f, g);
}
